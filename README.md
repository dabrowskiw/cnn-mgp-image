# CNN-MGP-image

Singularity image for CNN-MGP to allow reproducible analyses.

Build using:

```
sudo singularity build cnn-mgp.sif cnn-mgp.def
```

Run using:
```
singularity exec cnn-mgp.sif CNN-MGP -S <regular CNN-MGP parameters>
```

The ```-S``` will tell CNN-MGP not to look for a models directory in the current working directory but to use the models from the singularity image instead (see patch file). Without ```-S```, CNN-MGP will work as usual.

For development, test using:

```
sudo singularity build --sandbox cnn-mgp cnn-mgp.def
sudo singularity shell --writable cnn-mgp
```

Caution: For some reason, pip3 will not do ```python -m pip install tensorflow==2.3.0``` in ```%post```, however it works with ```tensorflow==2.5.0```. Since this also seems to work for cnn-mgp, that is the version of tensorflow used in this image. 
